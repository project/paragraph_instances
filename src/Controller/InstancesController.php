<?php

namespace Drupal\paragraph_instances\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Implements class InstancesController.
 */
class InstancesController extends ControllerBase {

  /**
   * The database connection to be used.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Dependency injection implementation.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack, LanguageManagerInterface $language_manager) {
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function findInstancesTab() {
    $path = $this->requestStack->getCurrentRequest()->getPathInfo();
    $arg = explode('/', $path);
    $paragraphType = $arg[4];
    $languages = $this->languageManager->getLanguages();
    $language = $this->requestStack->getCurrentRequest()->query->get('language');
    $nodes = $this->getInstances($paragraphType, $language);

    return [
      '#theme' => 'find-instances-tab',
      '#instances' => $nodes,
      '#languages' => $languages,
      '#language' => $language,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function findInstancesPage() {
    $paragraphTypes = ParagraphsType::loadMultiple();
    $paragraphType = $this->requestStack->getCurrentRequest()->query->get('type');
    $language = $this->requestStack->getCurrentRequest()->query->get('language');
    $nodes = $this->getInstances($paragraphType, $language);
    $languages = $this->languageManager->getLanguages();

    return [
      '#theme' => 'find-instances-page',
      '#instances' => $nodes,
      '#type' => $paragraphType,
      '#paragraphs' => $paragraphTypes,
      '#languages' => $languages,
      '#language' => $language,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getInstances($paragraphType, $language) {
    $database = $this->database;
    $nodes = [];
    $nids = [];

    if ($paragraphType) {
      if ($language) {
        $instances = $database->query('select * FROM {paragraphs_item_field_data} where status = 1 and type = :type and langcode = :language', [
          ':type' => $paragraphType,
          ':language' => $language,
        ])->fetchAll();
      }
      else {
        $instances = $database->query('select * FROM {paragraphs_item_field_data} where status = 1 and type = :type', [
          ':type' => $paragraphType,
        ])->fetchAll();
      }

      foreach ($instances as $instance) {
        // Check if the content still exists, because paragraphs_item_field_data
        // is apparently not cleaned up after deleting content.
        $uses = $database->query('select * FROM {' . ($instance->parent_type == 'paragraph' ? 'paragraph' : 'node') . '__' . $instance->parent_field_name . '} where ' . $instance->parent_field_name . '_target_id = :id', [
          ':id' => $instance->id,
        ])->fetch();
        if (empty($uses)) {
          continue;
        }

        if ($instance->parent_type == 'paragraph') {
          $nid = $this->getParentNodeId($instance->parent_id);
        }
        elseif ($instance->parent_type == 'node') {
          $nid = $instance->parent_id;
        }

        if ($nid) {
          if (!in_array($nid, $nids)) {
            $nids[] = $nid;
            $nodes[] = [
              'id' => $nid,
              'node' => $this->entityTypeManager->getStorage('node')->load($nid),
              'url' => 'https://' . $this->requestStack->getCurrentRequest()->getHost() . \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nid),
              'count' => 1,
            ];
          }
          else {
            foreach ($nodes as &$node) {
              if ($node['id'] == $nid) {
                $node['count']++;
                break;
              }
            }
          }
        }
      }
    }

    return $nodes;
  }

  /**
   * {@inheritdoc}
   */
  protected function getParentNodeId($id) {
    $database = $this->database;

    $instances = $database->query('select * FROM {paragraphs_item_field_data} where id = :id', [
      ':id' => $id,
    ])->fetchAll();

    if (!empty($instances) && isset($instances[0])) {
      if ($instances[0]->parent_type == 'node') {
        return $instances[0]->parent_id;
      }
      elseif ($instances[0]->parent_type == 'paragraph') {
        return $this->getParentNodeId($instances[0]->parent_id);
      }
    }

    return NULL;
  }

}
